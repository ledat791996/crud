import * as yargs from 'yargs';
export declare class CrudCreateCommand implements yargs.CommandModule {
    command: string;
    describe: string;
    protected static serviceTemplate(name: string): Promise<void>;
    protected static createFile(filePath: string, content: string, override?: boolean): Promise<void>;
    builder(args: yargs.Argv): any;
    handler(args: yargs.Arguments<any & {
        path: string;
    }>): void;
}
