import * as yargs from 'yargs';
import { CaseConverterEnum, generateTemplateFiles } from 'generate-template-files';
import path from 'path';
import fs from 'fs';
export class CrudCreateCommand implements yargs.CommandModule {
  command = 'create <path>';

  describe = 'Creates a new CRUD Folder.';

  protected static async serviceTemplate(name: string): Promise<void> {
    await generateTemplateFiles([
      {
        option: name,
        defaultCase: CaseConverterEnum.None,
        entry: {
          folderPath: __dirname + '/../../src/templates/',
        },
        output: {
          path: './src/resource/__name__',
          pathAndFileNameDefaultCase: CaseConverterEnum.CamelCase,
          overwrite: false,
        },
        dynamicReplacers: [
          {
            slot: '__name__',
            slotValue: name,
          },
        ],
      },
    ]);
  }

  protected static async createFile(filePath: string, content: string, override = true): Promise<void> {
    return new Promise<void>((ok, fail) => {
      if (override === false && fs.existsSync(filePath)) return ok();

      fs.writeFile(filePath, content, (err) => (err ? fail(err) : ok()));
    });
  }

  builder(args: yargs.Argv): any {
    return args.positional('path', {
      type: 'string',
      describe: 'Path of the CRUD Folder',
      demandOption: true,
    });
  }

  handler(args: yargs.Arguments<any & { path: string }>): void {
    try {
      const inputPath = 'src/' + args.path;
      const filename = path.basename(inputPath);
      CrudCreateCommand.serviceTemplate(filename);
    } catch (error) {
      console.error('error: ', error);
      process.exit(1);
    }
  }
}
