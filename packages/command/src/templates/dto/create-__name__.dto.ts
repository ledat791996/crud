import { PartialType } from '@nestjs/mapped-types';
import { __name__(pascalCase)Entity } from '../entities/__name__(camelCase).entity';
import { IsEmpty } from 'class-validator';
export class Create__name__(pascalCase)Dto extends PartialType(__name__(pascalCase)Entity) {
  @IsEmpty()
  id?: number;
}
