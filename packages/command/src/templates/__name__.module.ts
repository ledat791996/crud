import { Module } from '@nestjs/common';
import { __name__(pascalCase)Service } from './__name__(camelCase).service';
import { __name__(pascalCase)Controller } from './__name__(camelCase).controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { __name__(pascalCase)Entity } from './entities/__name__(camelCase).entity';

@Module({
  imports: [TypeOrmModule.forFeature([__name__(pascalCase)Entity])],
  controllers: [__name__(pascalCase)Controller],
  exports: [__name__(pascalCase)Service],
  providers: [__name__(pascalCase)Service],
})
export class __name__(pascalCase)Module {}
